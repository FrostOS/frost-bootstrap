#!/bin/bash
. $(dirname $0)/Variables.sh
mkdir $Working/binutils-pass1/binutils-build -p
tar -xvf $SRC/binutils-*.tar.xz --strip-components=1 -C $Working/binutils-pass1
pushd $Working/binutils-pass1/binutils-build

../configure --prefix=/tools --with-sysroot=${FROSTOS} \
--target=${FROST_TGT} --disable-nls --disable-werror --enable-multilib \
 --with-lib-path=/tools/lib32:/tools/lib

 make -j18
 make install
 mkdir /tools/lib
 ln -s lib /tools/lib64
 mkdir /tools/lib32
 popd