#!/bin/bash
. $(dirname $0)/MyScripts/Variables.sh

set +h
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

cat > ~/.bashrc << "EOF"
set +h
umask 022
LC_ALL=POSIX
export rootdrive=/dev/sdb5
export FROSTOS=/mnt/frostos
export FROST_TGT=x86_64-frostos-linux-gnu
export PATH=/tools/bin:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

export SRC=$FROSTOS/Sources
export Patches=$FROSTOS/Patches
export Working=$FROSTOS/Working

export MAKEFLAGS="-j9"

EOF

source ~/.bash_profile
cd $FROSTOS