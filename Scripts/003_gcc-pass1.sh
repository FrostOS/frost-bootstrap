#!/bin/bash
. $(dirname $0)/Variables.sh
mkdir -p $Working/gcc-pass1/build
tar -xvf $SRC/gcc-*.tar.xz --strip-components=1 -C $Working/gcc-pass1
pushd $Working/gcc-pass1
tar -xf $SRC/mpfr-*.tar.xz --strip-components=1 -C mpfr
tar -xf $SRC/gmp*.tar.xz --strip-components=1 -C gmp
tar -xf $SRC/mpc*.tar.xz --strip-components=1 -C mpc

patch -Np1 -i ${Patches}/gcc-pure64.patch
 sed -i "s|\$(if \$(wildcard \$(shell echo \$(SYSTEM_HEADER_DIR))/../../usr/lib32),../lib32)|../lib32|" gcc/config/i386/t-linux64
 for file in \
     $(find gcc/config -name linux64.h -o -name linux.h -o -name sysv4.h)
    do
      cp -u $file{,.orig}
      sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
          -e 's@/usr@/tools@g' $file.orig > $file
      echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> $file
      touch $file.orig
    done
popd

pushd $Working/gcc-pass1/build
../configure                                       \
--target=${FROST_TGT} \
        --prefix=/tools                                \
        --with-glibc-version=2.11                      \
--with-sysroot=${FROSTOS} \
  --with-newlib                                  \
        --without-headers                              \
        --with-local-prefix=/tools                     \
        --with-native-system-header-dir=/tools/include \
        --disable-nls                                  \
        --disable-shared                               \
        --disable-decimal-float                        \
        --disable-threads                              \
        --disable-libatomic                            \
        --disable-libgomp                              \
--disable-libmpx  \
 --disable-libquadmath                          \
        --disable-libssp                               \
        --disable-libvtv                               \
        --disable-libstdcxx                            \
        --enable-multilib                              \
--enable-languages=c,c++
make -j18
make -j1 install
popd
