#!/bin/bash
export rootdrive=/dev/sdb5
export FROSTOS=/mnt/frostos
export FROST_TGT=x86_64-frostos-linux-gnu
export PATH=/tools/bin:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

export SRC=$FROSTOS/Sources
export Patches=$FROSTOS/Patches
export Working=$FROSTOS/Working

export MAKEFLAGS="-j4"
set +h

