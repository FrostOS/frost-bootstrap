#!/bin/bash
. $(dirname $0)/Variables.sh
mkdir -p $Working/inux-headers-1
tar -xvf $SRC/linux-*.tar.xz -C $Working/inux-headers-1 --strip-components=1

pushd $Working/inux-headers-1  
   make mrproper
    make INSTALL_HDR_PATH=dest headers_install
    cp -rv dest/include/* /tools/include
popd

